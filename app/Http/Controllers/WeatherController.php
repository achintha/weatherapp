<?php

namespace App\Http\Controllers;

use Gmopx\LaravelOWM\LaravelOWM;
use Illuminate\Http\Request;
use Mockery\Exception;


class WeatherController extends Controller
{

    /**
     * WIll render the form, to enter the city name to be searched
     *
     * @return void
     */
    public function getCitySearch(){
        return view('weather');
    }

    /**
     * Will return weather information for the given city
     *
     * @param $city
     * @return array
     */
    protected function getWeatherInfo($city){
        try{
            $lowm = new LaravelOWM();
            $current_weather = $lowm->getCurrentWeather($city);

            if($current_weather->city->id == 0){
                // Throws and exception if service doesn't provide information for the given city
                //TODO give specific error msg, if the city cannot be found
                throw new Exception();
            }
            return [
                'err' =>false,
                'data'=>$current_weather,
                'msg' => ""
            ];
        } catch(Exception $e) {
            return [
                'err' =>true,
                'data'=> [],
                'msg' => "Something went wrong. please try again."
            ];
        }
    }
}
