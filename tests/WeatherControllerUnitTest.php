<?php


class WeatherControllerUnitTest extends TestCase
{
    /**
     * to test if the landing page is correct
     */
    public function test_main_landing_page_exists()
    {
        $this->visit('/')
            ->see('Weather Information');
    }

    /**
     * to test if the city is passed with the response
     */
    public function test_weather_info_returns_expected_data()
    {
        $response = $this->call('GET','data/london');
        $this->assertEquals('London',$response->original['data']->city->name);
        $this->assertArrayHasKey('temperature',(Array)$response->original['data']);
        $this->assertArrayHasKey('lastUpdate',(Array)$response->original['data']);
        $this->assertArrayHasKey('sun',(Array)$response->original['data']);
    }

    /**
     * to test the data route works
     */
    public function test_data_retrieval_works()
    {
        $this->call('GET','data/london');
        $this->assertResponseOk();
    }


}