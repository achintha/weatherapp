## About Weather app

Below given are the sources used during the development process

- [On considering what api service to be used](https://stackoverflow.com/questions/43137198/weather-api-for-laravel-5-4-does-anyone-knows-any-weather-api-for-laravel-5-4).
- [Wrapper used for retrieving data from open weather map API](https://packagist.org/packages/gmopx/laravel-owm).
- [Tutorial used for Unit tests](https://laracasts.com/lessons/tdd-by-example).

## Special Notes

- Intentionally hardcoded keys into the code (without using env variables)
- Observed that the respond from the service does not return some of the data all the time (eg: temperatures).
- If the city name is miss-spelled, API returns data to the closest matching city name. 
This may sometimes give unexpected results. (eg:Search "test" will result "Tierce [FR]")  