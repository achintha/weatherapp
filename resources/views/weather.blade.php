<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Weather Information</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{{ asset('css/custom.css') }}}" rel="stylesheet" type="text/css">

        <!-- Scrips -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <script type="text/javascript" src="{{{ asset('js/custom.js') }}}"></script>
    </head>
    <body>

        <div class="content">
            <div class="title m-b-md">
                Weather Information
            </div>
        </div>
        <form>
            <div class="position-ref full-height">
                <div class="content" style="display: none">
                    <div class="err">
                        Please enter a city to get weather information.
                    </div>
                </div>

                <div class="err-container content">
                    <div class="element">
                        <div class="err" id="err"></div>
                    </div>
                </div>

                <div class="content">
                    <div class="element">
                        <input type=”text” id="city" name=”city” required>
                        <button
                                id="get_info_btn"
                                onclick="getWeatherInfo()"
                                type="button"
                                placeholder="Enter City"
                                class="button">Get Weather Info</button>
                    </div>
                </div>
                <div class="results content">
                    <div class="element">
                        <div class="city" id="data_city"></div>
                    </div>
                </div>
                <div class="results content">
                    <div class="element">
                        <div class="column key"> Weather Description : </div>
                        <div class="column info" id="data_weather_desc"></div>
                    </div>
                </div>
                <div class="results content">
                    <div class="element">
                        <div class="column key"> Current Temperature : </div>
                        <div class="column info" id="data_temp_curr"></div>
                    </div>
                </div>
                <div class="results content">
                    <div class="element">
                        <div class="column key"> Max Temperature of the Day : </div>
                        <div class="column info" id="data_temp_max"></div>
                    </div>
                </div>
                <div class="results content">
                    <div class="element">
                        <div class="column key"> Min Temperature of the Day : </div>
                        <div class="column info" id="data_temp_min"></div>
                    </div>
                </div>
                <div class="results content">
                    <div class="element">
                        <div class="column key"> Sun Rises at : </div>
                        <div class="column info" id="data_sun_rise"></div>
                    </div>
                </div>
                <div class="results content">
                    <div class="element">
                        <div class="column key"> Sun Set by : </div>
                        <div class="column info" id="data_sun_set"></div>
                    </div>
                </div>
                <div class="results content">
                    <div class="element">
                        <div class="column key"> Latest Update time : </div>
                        <div class="column info" id="data_latest_update"></div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
