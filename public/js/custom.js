$(document).ready(function($){
    $("#city").attr('required','required');
    $(".results").hide();
});

function getWeatherInfo(){
    var city = $("#city").val();
    if(city) {
        $("#err").hide();
        $.ajax({
            url: "/data/" + city ,
            context: document.body
        }).done(function(result) {
            console.log(result);
            if(result.err == false) {
                $(".results").show();
                $("#data_city").text(result.data.city.name+" ["+result.data.city.country+"]");
                $("#data_weather_desc").text(result.data.weather.description);
                $("#data_temp_curr").text(result.data.temperature.now.value);
                $("#data_temp_max").text(result.data.temperature.min.value);
                $("#data_temp_min").text(result.data.temperature.max.value);
                $("#data_latest_update").text(result.data.lastUpdate.date);
                $("#data_sun_rise").text(result.data.sun.rise.date);
                $("#data_sun_set").text(result.data.sun.set.date);
            } else {
                $("#err").show().text(result.msg);
            }
        });
    } else {
        $("#err").show().text("Please enter a valid string.");
        $(".results").hide();
    }
}